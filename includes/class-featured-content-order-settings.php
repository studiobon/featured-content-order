<?php

if ( ! defined( 'ABSPATH' ) ) exit;

class Featured_Content_Order_Settings {

  /**
   * The single instance of Featured_Content_Order_Settings.
   * @var   object
   * @access  private
   * @since   1.0.0
   */
  private static $_instance = null;

  /**
   * The main plugin object.
   * @var   object
   * @access  public
   * @since   1.0.0
   */
  public $parent = null;

  /**
   * Prefix for plugin settings.
   * @var     string
   * @access  public
   * @since   1.0.0
   */
  public $base = '';

  /**
   * Available settings for plugin.
   * @var     array
   * @access  public
   * @since   1.0.0
   */
  public $settings = array();

  public function __construct ( $parent ) {
    $this->parent = $parent;

    $this->base = 'wpt_';

    // Initialise settings
    add_action( 'init', array( $this, 'init_settings' ), 11 );

    // Register plugin settings
    add_action( 'admin_init' , array( $this, 'register_settings' ) );

    // Add settings page to menu
    add_action( 'admin_menu' , array( $this, 'add_menu_item' ) );

    // Add settings link to plugins page
    add_filter( 'plugin_action_links_' . plugin_basename( $this->parent->file ) , array( $this, 'add_settings_link' ) );
  }

  /**
   * Initialise settings
   * @return void
   */
  public function init_settings () {
    $this->settings = $this->settings_fields();
  }

  /**
   * Add settings page to admin menu
   * @return void
   */
  public function add_menu_item () {
    $page = add_object_page( __( 'Featured Content', 'featured-content-order' ) , __( 'Featured Content', 'featured-content-order' ) , 'manage_options' , $this->parent->_token . '_settings' ,  array( $this, 'settings_page' ), 'dashicons-grid-view' );
    add_action( 'admin_print_styles-' . $page, array( $this, 'settings_assets' ) );
  }

  /**
   * Load settings JS & CSS
   * @return void
   */
  public function settings_assets () {
    wp_register_script( $this->parent->_token . '-settings-js', $this->parent->assets_url . 'js/settings' . $this->parent->script_suffix . '.js', array('jquery' ), '1.0.0' );
    wp_enqueue_script( 'jquery-ui-sortable' );
    wp_enqueue_script( $this->parent->_token . '-settings-js' );
    wp_enqueue_style( $this->parent->_token . '-admin' );
  }

  /**
   * Add settings link to plugin list table
   * @param  array $links Existing links
   * @return array    Modified links
   */
  public function add_settings_link ( $links ) {
    $settings_link = '<a href="options-general.php?page=' . $this->parent->_token . '_settings">' . __( 'Settings', 'featured-content-order' ) . '</a>';
      array_push( $links, $settings_link );
      return $links;
  }

  /**
   * Build settings fields
   * @return array Fields to be displayed on settings page
   */
  private function settings_fields () {

    $settings['standard'] = array(
      'title'         => __( 'Featured Content Order', 'featured-content-order' ),
      'description'     => __( 'These are fairly standard form input fields.', 'featured-content-order' ),
      'fields'        => array(
        array(
          'id'      => 'text_field',
          'label'     => __( 'Some Text' , 'featured-content-order' ),
          'description' => __( 'This is a standard text field.', 'featured-content-order' ),
          'type'      => 'text',
          'default'   => '',
          'placeholder' => __( 'Placeholder text', 'featured-content-order' )
        ),

        array(
          'id'      => 'secret_text_field',
          'label'     => __( 'Some Secret Text' , 'featured-content-order' ),
          'description' => __( 'This is a secret text field - any data saved here will not be displayed after the page has reloaded, but it will be saved.', 'featured-content-order' ),
          'type'      => 'text_secret',
          'default'   => '',
          'placeholder' => __( 'Placeholder text', 'featured-content-order' )
        )

      )
    );

    $settings = apply_filters( $this->parent->_token . '_settings_fields', $settings );

    return $settings;
  }

  /**
   * Register plugin settings
   * @return void
   */
  public function register_settings () {
    if ( is_array( $this->settings ) ) {

      // Check posted/selected tab
      $current_section = '';
      if ( isset( $_POST['tab'] ) && $_POST['tab'] ) {
        $current_section = $_POST['tab'];
      } else {
        if ( isset( $_GET['tab'] ) && $_GET['tab'] ) {
          $current_section = $_GET['tab'];
        }
      }

      foreach ( $this->settings as $section => $data ) {

        if ( $current_section && $current_section != $section ) continue;

        // Add section to page
        add_settings_section( $section, $data['title'], array( $this, 'settings_section' ), $this->parent->_token . '_settings' );

        foreach ( $data['fields'] as $field ) {

          // Validation callback for field
          $validation = '';
          if ( isset( $field['callback'] ) ) {
            $validation = $field['callback'];
          }

          // Register field
          $option_name = $this->base . $field['id'];
          register_setting( $this->parent->_token . '_settings', $option_name, $validation );

          // Add field to page
          add_settings_field( $field['id'], $field['label'], array( $this->parent->admin, 'display_field' ), $this->parent->_token . '_settings', $section, array( 'field' => $field, 'prefix' => $this->base ) );
        }

        if ( ! $current_section ) break;
      }
    }
  }

  public function settings_section ( $section ) {
    $html = '<p> ' . $this->settings[ $section['id'] ]['description'] . '</p>' . "\n";
    echo $html;
  }

  /**
   * Load settings page content
   * @return void
   */
  public function settings_page () {

    if (!current_user_can('manage_options')) {
      wp_die( __('You do not have sufficient permissions to access this page.') );
    }

    // See if the user has posted us some information
    if( isset($_POST[ $this->parent->_token ]) ) {      
      // Save the posted value in the database
      foreach ($_POST[ $this->parent->_token ] as $id => $value) {
        $this->parent->set_order( $id, $value );
      }

      // Put an settings updated message on the screen
      $updated = true;
    }

    $posts = array();

    $args = array( 'post_type' => 'post' 
                  ,'tag' => 'featured' 
                  ,'orderby' => 'meta_value_num'
                  ,'meta_key' => $this->parent->_token
                  ,'order' => 'ASC'
                  );

    $the_query = new WP_Query( $args );

    while ( $the_query->have_posts() ) : $the_query->the_post();
      // Read in existing option value from database
      $posts[get_the_ID()] = array( 'post' => $the_query->post, 
                                      'fields' => get_fields() 
                                      );
    endwhile;

    echo $this->load_plugin_template('form', $posts);

  }


  /**
   * Wrapper function to load a template file
   * @param  string $template_name   Template name
   * @return string                  Template contents
   */
  public function load_plugin_template ( $template_name, $args ) {

    $posts = $args;

    $file_path = esc_url($this->parent->dir . '/templates/' . $template_name . '.php');
    // Check for file
    if( ! file_exists($file_path) ) return;

    ob_start();
    include( $file_path );
    $ret = ob_get_contents();
    ob_end_clean();
    
    return $ret;

  }

  /**
   * Main Featured_Content_Order_Settings Instance
   *
   * Ensures only one instance of Featured_Content_Order_Settings is loaded or can be loaded.
   *
   * @since 1.0.0
   * @static
   * @see Featured_Content_Order()
   * @return Main Featured_Content_Order_Settings instance
   */
  public static function instance ( $parent ) {
    if ( is_null( self::$_instance ) ) {
      self::$_instance = new self( $parent );
    }
    return self::$_instance;
  } // End instance()

  /**
   * Cloning is forbidden.
   *
   * @since 1.0.0
   */
  public function __clone () {
    _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' ), $this->parent->_version );
  } // End __clone()

  /**
   * Unserializing instances of this class is forbidden.
   *
   * @since 1.0.0
   */
  public function __wakeup () {
    _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' ), $this->parent->_version );
  } // End __wakeup()

}
