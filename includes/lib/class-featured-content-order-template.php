<?php

if ( ! defined( 'ABSPATH' ) ) exit;

class Featured_Content_Order_Template {

  /**
   * The name for the template.
   * @var   string
   * @access  public
   * @since   1.0.0
   */
  public $template;

  public function __construct ( $template = '' ) {

    $file_path = esc_url($this->parent->dir . '/templates/' . $template_name . '.php');
    // Check for file
    if( ! file_exists($file_path) ) return;

    ob_start();
    include( $file_path );
    $ret = ob_get_contents();
    ob_end_clean();
    
    return $ret;

  }

}
