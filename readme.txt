=== Featured Content Order ===
Contributors: Afonso Duarte
Tags: wordpress, plugin, featured-content
Requires at least: 3.9
Tested up to: 4.0
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Manage the order of featured content.  

== Description ==

Adds a settings page where you can re-order featured content elements. 

== Installation ==

Installing "Featured Content Order" can be done either by searching for "Featured Content Order" via the "Plugins > Add New" screen in your WordPress dashboard, or by using the following steps:

1. Download the plugin via WordPress.org
1. Upload the ZIP file through the 'Plugins > Add New > Upload' screen in your WordPress dashboard
1. Activate the plugin through the 'Plugins' menu in WordPress

== Screenshots ==

1. Description of first screenshot named screenshot-1
2. Description of second screenshot named screenshot-2
3. Description of third screenshot named screenshot-3

== Changelog ==

= 1.0 =
* 15-12-2015
* Initial release
