<?php if(isset($updated)): ?>
<div class="updated">
  <p>
    <strong><?php _e('Cover Order saved.', 'menu-test' ); ?></strong>
  </p>
</div>
<?php endif; ?>

<div class="wrap" id="<?= $this->parent->_token ?>_settings">
  <h2><?= __( 'Featured Content Order' , 'featured-content-order' ) ?></h2>
  <form name="featured-content-order__form" method="post" action="" id="featured-content-order__form" enctype="multipart/form-data">
    <ul class="sortable">
    <?php foreach ($posts as $id => $post): ?>
    <?php $order = $this->parent->get_order( $id ); ?> 
    <li class="featured-content-order__item">
      <?php echo get_the_post_thumbnail( $id, array(154,154) ); ?> 
      <label for="<?= $this->parent->_token ?>[<?= $id ?>]" class="featured-content-order__label"><?= $post['post']->post_title ?></label>
      <input type="hidden" name="<?= $this->parent->_token ?>[<?= $id ?>]" id="<?= $this->parent->_token ?>[<?= $id ?>]" value="<?= $order ?>" size="20">
    </li>
    <?php endforeach; ?>  
    </ul>

    <p class="submit">
      <input name="Submit" type="submit" class="button-primary" value="<?= esc_attr( __( 'Save order' , 'featured-content-order' ) ) ?>" />
    </p>
  </form>
</div>
 
<?php /*
<input type="hidden" name="<?php echo $hidden_field_name; ?>" value="Y">

<ul class="sortable">
<?php foreach ($opt_val as $id => $item): ?>  
<?php $order = bon_cover_order( $id, 'get'); ?> 
<li class="front-page-template-<?= $item['fields']['front_page_template'] ?> cover-order-li">
  <?php echo get_the_post_thumbnail( $id, array(154,154) ); ?> 
  <label for="<?= $data_field_name ?>[<?= $id ?>]"><?= $item['post']->post_title ?></label>
  <input type="hidden" name="<?= $data_field_name ?>[<?= $id ?>]" id="<?= $data_field_name ?>[<?= $id ?>]" value="<?= $order ?>" size="20">
</li>
<?php endforeach; ?>  
</ul>
<hr />

<p class="submit">
<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Save Changes') ?>" />
</p>

</form>

*/ ?>