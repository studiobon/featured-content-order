<?php
/*
 * Plugin Name: Featured Content Order
 * Version: 1.0
 * Plugin URI: http://www.studiobon.com/
 * Description: A plugin that allows manual ordering of featured content.
 * Author: Afonso Duarte
 * Author URI: http://afn.so/
 * Requires at least: 4.0
 * Tested up to: 4.0
 *
 * Text Domain: featured-content-order
 * Domain Path: /lang/
 *
 * @package WordPress
 * @author Afonso Duarte
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

// Load plugin class files
require_once( 'includes/class-featured-content-order.php' );
require_once( 'includes/class-featured-content-order-settings.php' );

// Load plugin libraries
require_once( 'includes/lib/class-featured-content-order-admin-api.php' );
require_once( 'includes/lib/class-featured-content-order-post-type.php' );
require_once( 'includes/lib/class-featured-content-order-taxonomy.php' );
require_once( 'includes/lib/class-featured-content-order-template.php' );


/**
 * Returns the main instance of Featured_Content_Order to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object Featured_Content_Order
 */
function Featured_Content_Order () {
	$instance = Featured_Content_Order::instance( __FILE__, '1.0.0' );

	if ( is_null( $instance->settings ) ) {
		$instance->settings = Featured_Content_Order_Settings::instance( $instance );
	}

	return $instance;
}

Featured_Content_Order();
