jQuery(document).ready(function($) {

  $('.sortable')
    .sortable({
      stop: function(event, ui) { 
        $('.sortable li input').each(function(e){
          $(this).attr('value', e+1);
        });
      }
    })
    .disableSelection();

});